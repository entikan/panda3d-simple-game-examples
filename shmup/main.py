import sys
from random import randint, choice, uniform
from direct.showbase.ShowBase import ShowBase
from panda3d.core import TextNode, NodePath, LineSegs, Vec3


base = ShowBase()
base.accept("escape", sys.exit)
base.win.set_clear_color((0.1,0.1,0.1,1))
base.cam.set_pos_hpr(0,0,128,0,-90,0)

base.info = base.a2dTopLeft.attach_new_node(TextNode("info"))
base.info.set_scale(0.04)
base.info.node().text = """\nSimple SHMUP Example.
\nAWSD to move. \nEscape to exit.
"""

linesegs = LineSegs("polyline")
def polyline(parent, points, loop=True):
    linesegs.move_to(points[0])
    for point in points[1:]: linesegs.draw_to(point)
    if loop: linesegs.draw_to(points[0])
    return parent.attach_new_node(linesegs.create())

bullet_line = polyline(NodePath('bullet'), [(0,0,0),(0,1,0)], loop=False)
triang_line = polyline(NodePath('triang'), [(0,1,0),(0.5,-1,0),(-0.5,-1,0)])


def starfield():
    for i in range(512):
        pos = Vec3(randint(-64,64),randint(-256,256),randint(-128,-64))
        linesegs.move_to(pos)
        linesegs.draw_to(pos+(0,0.2,0))
    np = render.attach_new_node('starfield')
    np_a = np.attach_new_node(linesegs.create())
    np_b = np_a.copy_to(np)
    np_b.set_y(512)
    np.posInterval(16, (0,-512,0)).loop()
starfield()


class Bullet:
    def __init__(self, pos, hpr, to_hurt=[], velocity=(0,64,0)):
        self.to_hurt = to_hurt
        self.velocity = Vec3(velocity)
        self.path = render.attach_new_node("bullet")
        self.shape = bullet_line.copy_to(self.path)
        self.path.set_pos_hpr(pos, hpr)
        base.task_mgr.add(self.update)

    def update(self, task):
        if self.path.get_distance(render) > 64:
            self.path.detach_node()
            return task.done
        for to_hurt in self.to_hurt:
            if self.path.get_distance(to_hurt.path) < 1:
                self.path.detach_node()
                to_hurt.hp -= 1
                if to_hurt.hp <= 0:
                    to_hurt.die()
                return task.done
        self.path.set_pos(self.path, self.velocity*base.clock.dt)
        return task.cont


class Enemy:
    def __init__(self, pos, chasing):
        self.hp, self.speed = 1, 20
        self.chasing, self.velocity = chasing, Vec3()
        self.path = render.attach_new_node("enemy")
        self.path.set_pos(pos)
        self.path.set_color((1,0,0,1), 1)
        self.body = triang_line.copy_to(self.path)
        self.body.set_hpr((0,180,0))
        base.task_mgr.add(self.update)

    def die(self):
        if self in base.enemies: base.enemies.remove(self)

    def ai(self):
        self.velocity.y -= self.speed*base.clock.dt
        self.velocity.x -= int(self.path.get_x() > self.chasing.path.get_x())*base.clock.dt
        self.velocity.x += int(self.path.get_x() < self.chasing.path.get_x())*base.clock.dt
        self.path.set_pos(self.path, self.velocity)
        self.velocity *= 0.1*base.clock.dt
        if self.path.get_distance(self.chasing.path) < self.body.get_sx(): self.chasing.die()
        if self.path.get_y() < -64: self.die()

    def update(self, task):
        self.ai()
        if not self in base.enemies:
            self.path.detach_node()
            return task.done
        return task.cont


class EnemyBig(Enemy):
    def __init__(self, pos, chasing):
        super().__init__(pos, chasing)
        self.hp, self.speed = 2, 16
        self.body.set_scale(3)
        self.turret = triang_line.copy_to(self.path)
        self.bullet_cooldown = 1

    def ai(self):
        super().ai()
        self.turret.look_at(render, self.chasing.path.get_pos())
        self.bullet_cooldown -= base.clock.dt
        if self.bullet_cooldown <= 0:
            self.bullet_cooldown = 0.5 + uniform(0,0.1)
            bullet = Bullet(self.path.get_pos(), self.turret.get_hpr(), to_hurt=[self.chasing])
            bullet.shape.set_color((1,0.5,0.5,1),1)


class Player:
    def __init__(self):
        self.hp = 1
        self.velocity = Vec3(0,0,0)
        self.path = render.attach_new_node("player")
        self.shape = polyline(self.path, [
            (-1.5,-0.2,0),(-1.8,1.3,0),(-0.5,0.5,0),(0,2,0),#left half
            (0.5,0.5,0),(1.8,1.3,0),(1.5,-0.2,0), (0,0.3,0),#right half
        ])
        base.task_mgr.add(self.update)
        self.die()

    def die(self):
        self.bullet_cooldown = 2
        self.path.set_pos((0,0,0))
        base.enemies = []

    def update(self, task):
        if not randint(0,int(4096*base.clock.dt)):
            if randint(0,4):
                base.enemies.append(Enemy((randint(-16,16), 40, 0), self))
            else:
                base.enemies.append(EnemyBig((randint(-16,16), 40, 0), self))

        button = base.mouseWatcherNode.is_button_down
        velocity = Vec3(
            int(button("d"))-int(button("a")),
            int(button("w"))-int(button("s")),0)
        self.velocity += velocity*2*base.clock.dt
        self.path.set_pos(self.path, self.velocity)
        self.shape.set_r(self.velocity.x*100)
        self.velocity *= 0.01**base.clock.dt
        self.bullet_cooldown -= base.clock.dt
        if self.bullet_cooldown <= 0:
            self.bullet_cooldown = 0.2
            Bullet(self.path.get_pos(), self.shape.get_hpr(), to_hurt=base.enemies)
        return task.cont


Player()
base.run()
